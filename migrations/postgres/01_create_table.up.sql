CREATE TABLE "brand" (
  "id" UUID PRIMARY KEY ,
  "photo" varchar,
  "name" varchar(45),
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

CREATE TABLE "category" (
  "id" UUID PRIMARY KEY,
  "name" varchar(45),
  "parent_id" UUID ,
  "brand_id" UUID NOT NULL,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);



CREATE TABLE "product" (
  "id" UUID PRIMARY KEY ,
  "photo" varchar,
  "name" varchar(45),
  "category_id" UUID NOT NULL,
  "brand_id" UUID NOT NULL,
  "bar_code" varchar unique,
  "price" NUMERIC ,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);