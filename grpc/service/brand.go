package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/final_exam2/product_service/config"
	"gitlab.com/final_exam2/product_service/genproto/product_service"
	"gitlab.com/final_exam2/product_service/grpc/client"
	"gitlab.com/final_exam2/product_service/pkg/logger"
	"gitlab.com/final_exam2/product_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type brandService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*product_service.UnimplementedBrandServiceServer
}

func NewBrandService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *brandService {
	return &brandService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *brandService) Create(ctx context.Context, req *product_service.BrandCreate) (*product_service.Brand, error) {
	u.log.Info("====== Brand Create ======", logger.Any("req", req))

	resp, err := u.strg.Brand().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Brand: u.strg.Brand().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *brandService) GetById(ctx context.Context, req *product_service.BrandPrimaryKey) (*product_service.Brand, error) {
	u.log.Info("====== Brand Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Brand().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Brand Get By ID: u.strg.Brand().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *brandService) GetList(ctx context.Context, req *product_service.BrandGetListRequest) (*product_service.BrandGetListResponse, error) {
	u.log.Info("====== Brand Get List ======", logger.Any("req", req))

	resp, err := u.strg.Brand().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Brand Get List: u.strg.Brand().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *brandService) Update(ctx context.Context, req *product_service.BrandUpdate) (*product_service.Brand, error) {
	u.log.Info("====== Brand Update ======", logger.Any("req", req))

	resp, err := u.strg.Brand().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Brand Update: u.strg.Brand().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *brandService) Delete(ctx context.Context, req *product_service.BrandPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Brand Delete ======", logger.Any("req", req))

	err := u.strg.Brand().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Brand Delete: u.strg.Brand().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
