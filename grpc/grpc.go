package grpc

import (
	"gitlab.com/final_exam2/product_service/config"
	"gitlab.com/final_exam2/product_service/genproto/product_service"
	"gitlab.com/final_exam2/product_service/grpc/client"
	"gitlab.com/final_exam2/product_service/grpc/service"
	"gitlab.com/final_exam2/product_service/pkg/logger"
	"gitlab.com/final_exam2/product_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	product_service.RegisterBrandServiceServer(grpcServer, service.NewBrandService(cfg, log, strg, srvc))
	product_service.RegisterCategoryServiceServer(grpcServer, service.NewCategoryService(cfg, log, strg, srvc))
	product_service.RegisterProductServiceServer(grpcServer, service.NewProductService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
