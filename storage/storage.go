package storage

import (
	"context"

	"gitlab.com/final_exam2/product_service/genproto/product_service"
)

type StorageI interface {
	CloseDB()
	Brand() BrandRepoI
	Category() CategoryRepoI
	Product() ProductRepoI
}

type BrandRepoI interface {
	Create(context.Context, *product_service.BrandCreate) (*product_service.Brand, error)
	GetByID(context.Context, *product_service.BrandPrimaryKey) (*product_service.Brand, error)
	GetList(context.Context, *product_service.BrandGetListRequest) (*product_service.BrandGetListResponse, error)
	Update(context.Context, *product_service.BrandUpdate) (*product_service.Brand, error)
	Delete(context.Context, *product_service.BrandPrimaryKey) error
}

type CategoryRepoI interface {
	Create(context.Context, *product_service.CategoryCreate) (*product_service.Category, error)
	GetByID(context.Context, *product_service.CategoryPrimaryKey) (*product_service.Category, error)
	GetList(context.Context, *product_service.CategoryGetListRequest) (*product_service.CategoryGetListResponse, error)
	Update(context.Context, *product_service.CategoryUpdate) (*product_service.Category, error)
	Delete(context.Context, *product_service.CategoryPrimaryKey) error
}

type ProductRepoI interface {
	Create(context.Context, *product_service.ProductCreate) (*product_service.Product, error)
	GetByID(context.Context, *product_service.ProductPrimaryKey) (*product_service.Product, error)
	GetList(context.Context, *product_service.ProductGetListRequest) (*product_service.ProductGetListResponse, error)
	Update(context.Context, *product_service.ProductUpdate) (*product_service.Product, error)
	Delete(context.Context, *product_service.ProductPrimaryKey) error
}
