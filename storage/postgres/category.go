package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/final_exam2/product_service/genproto/product_service"
	"gitlab.com/final_exam2/product_service/pkg/helper"
)

type CategoryRepo struct {
	db *pgxpool.Pool
}

func NewCategoryRepo(db *pgxpool.Pool) *CategoryRepo {
	return &CategoryRepo{
		db: db,
	}
}

func (r *CategoryRepo) Create(ctx context.Context, req *product_service.CategoryCreate) (*product_service.Category, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO category(id, name,parent_id,brand_id,updated_at)
		VALUES ($1, $2, $3,$4, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Name,
		helper.NewNullString(req.ParentId),
		helper.NewNullString(req.BrandId),
	)

	if err != nil {
		return nil, err
	}

	return &product_service.Category{
		Id:       id,
		Name:     req.Name,
		ParentId: req.ParentId,
		BrandId:  req.BrandId,
	}, nil
}

func (r *CategoryRepo) GetByID(ctx context.Context, req *product_service.CategoryPrimaryKey) (*product_service.Category, error) {

	var (
		query string

		id         sql.NullString
		name       sql.NullString
		parent_id  sql.NullString
		brand_id   sql.NullString
		created_at sql.NullString
		updated_at sql.NullString
	)

	query = `
		SELECT
			id,
			name,
			parent_id,
			brand_id,
			created_at,
			updated_at		
		FROM category
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&parent_id,
		&brand_id,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &product_service.Category{
		Id:        id.String,
		Name:      name.String,
		ParentId:  parent_id.String,
		BrandId:   brand_id.String,
		CreatedAt: created_at.String,
		UpdatedAt: updated_at.String,
	}, nil
}

func (r *CategoryRepo) GetList(ctx context.Context, req *product_service.CategoryGetListRequest) (*product_service.CategoryGetListResponse, error) {

	var (
		resp   = &product_service.CategoryGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			parent_id,
			brand_id,
			created_at,
			updated_at		
		FROM category
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			name       sql.NullString
			parent_id  sql.NullString
			brand_id   sql.NullString
			created_at sql.NullString
			updated_at sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&parent_id,
			&brand_id,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Categories = append(resp.Categories, &product_service.Category{
			Id:        id.String,
			Name:      name.String,
			ParentId:  parent_id.String,
			BrandId:   brand_id.String,
			CreatedAt: created_at.String,
			UpdatedAt: updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *CategoryRepo) Update(ctx context.Context, req *product_service.CategoryUpdate) (*product_service.Category, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
		category
		SET
			name = :name,
			parent_id = :parent_id,
			brand_id = :brand_id,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":        req.GetId(),
		"name":      req.GetName(),
		"parent_id": req.GetParentId(),
		"brand_id":  req.GetBrandId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &product_service.Category{
		Id:       req.Id,
		Name:     req.Name,
		ParentId: req.ParentId,
		BrandId:  req.BrandId,
	}, nil
}

func (r *CategoryRepo) Delete(ctx context.Context, req *product_service.CategoryPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM category WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
