package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/final_exam2/product_service/genproto/product_service"
	"gitlab.com/final_exam2/product_service/pkg/helper"
)

type ProductRepo struct {
	db *pgxpool.Pool
}

func NewProductRepo(db *pgxpool.Pool) *ProductRepo {
	return &ProductRepo{
		db: db,
	}
}

func (r *ProductRepo) Create(ctx context.Context, req *product_service.ProductCreate) (*product_service.Product, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO product(id,photo, name, category_id  , brand_id, bar_code, price, updated_at)
		VALUES ($1, $2, $3,$4,$5 ,$6, $7,NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Photo,
		req.Name,
		req.CategoryId,
		req.BrandId,
		req.BarCode,
		req.Price,
	)

	if err != nil {
		return nil, err
	}

	return &product_service.Product{
		Id:         id,
		Photo:      req.Photo,
		Name:       req.Name,
		CategoryId: req.CategoryId,
		BrandId:    req.BrandId,
		BarCode:    req.BarCode,
		Price:      req.Price,
	}, nil
}

func (r *ProductRepo) GetByID(ctx context.Context, req *product_service.ProductPrimaryKey) (*product_service.Product, error) {

	var (
		query string

		id          sql.NullString
		photo       sql.NullString
		name        sql.NullString
		category_id sql.NullString
		brand_id    sql.NullString
		bar_code    sql.NullString
		price       sql.NullFloat64
		created_at  sql.NullString
		updated_at  sql.NullString
	)

	query = `
		SELECT
			id,
			photo,
			name,
			category_id,
			brand_id,
			bar_code,
			price,
			created_at,
			updated_at		
		FROM product
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&photo,
		&name,
		&category_id,
		&brand_id,
		&bar_code,
		&price,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &product_service.Product{
		Id:         id.String,
		Photo:      photo.String,
		Name:       name.String,
		CategoryId: category_id.String,
		BrandId:    brand_id.String,
		BarCode:    bar_code.String,
		Price:      price.Float64,
		CreatedAt:  created_at.String,
		UpdatedAt:  updated_at.String,
	}, nil
}

func (r *ProductRepo) GetList(ctx context.Context, req *product_service.ProductGetListRequest) (*product_service.ProductGetListResponse, error) {

	var (
		resp   = &product_service.ProductGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			photo,
			name,
			category_id,
			brand_id,
			bar_code,
			price,
			created_at,
			updated_at		
		FROM product
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			photo       sql.NullString
			name        sql.NullString
			category_id sql.NullString
			brand_id    sql.NullString
			bar_code    sql.NullString
			price       sql.NullFloat64
			created_at  sql.NullString
			updated_at  sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&photo,
			&name,
			&category_id,
			&brand_id,
			&bar_code,
			&price,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Products = append(resp.Products, &product_service.Product{
			Id:         id.String,
			Photo:      photo.String,
			Name:       name.String,
			CategoryId: category_id.String,
			BrandId:    brand_id.String,
			BarCode:    bar_code.String,
			Price:      price.Float64,
			CreatedAt:  created_at.String,
			UpdatedAt:  updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *ProductRepo) Update(ctx context.Context, req *product_service.ProductUpdate) (*product_service.Product, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
		product
		SET
			photo = :photo,
			name = :name,
			category_id = :category_id,
			brand_id = :brand_id,
			bar_code = :bar_code,
			price = :price,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":          req.GetId(),
		"photo":       req.GetPhoto(),
		"name":        req.GetName(),
		"category_id": req.GetCategoryId(),
		"brand_id":    req.GetBrandId(),
		"bar_code":    req.GetBarCode(),
		"price":       req.GetPrice(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &product_service.Product{
		Id:         req.Id,
		Photo:      req.Photo,
		Name:       req.Name,
		CategoryId: req.CategoryId,
		BrandId:    req.BrandId,
		BarCode:    req.BarCode,
		Price:      req.Price,
	}, nil
}

func (r *ProductRepo) Delete(ctx context.Context, req *product_service.ProductPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM product WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
